package com.gkemayo.hateoas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gkemayo.hateoas.entity.Account;

public interface AccountRepository extends JpaRepository<Account, Long> {
	
}
