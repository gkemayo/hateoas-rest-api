package com.gkemayo.hateoas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gkemayo.hateoas.entity.Operation;

public interface OperationRepository extends JpaRepository<Operation, Long> {
	
}
