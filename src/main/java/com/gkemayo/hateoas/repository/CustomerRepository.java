package com.gkemayo.hateoas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gkemayo.hateoas.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
	
}
