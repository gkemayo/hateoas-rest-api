package com.gkemayo.hateoas.entity.representationmodel;

import java.util.Date;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gkemayo.hateoas.entity.OperationType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Relation(value = "operation", collectionRelation="operations")
public class OperationModel extends RepresentationModel<OperationModel>{

	private Long id;

	private String label;
	
	private Double amount;
	
	private Date operationDate;

	private OperationType type;
	
}
