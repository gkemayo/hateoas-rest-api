package com.gkemayo.hateoas.entity.representationmodel;

import java.util.Date;
import java.util.List;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Relation(value = "customer", collectionRelation="customers")
public class CustomerModel extends RepresentationModel<CustomerModel> {
	
	private Long id;
	
	private String identifier;
	
	private String fullName;
	
	private Date birthDate;
	
	private List<AccountModel> accounts;

}
