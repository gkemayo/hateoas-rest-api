package com.gkemayo.hateoas.entity.representationmodel;

import java.util.Date;
import java.util.List;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gkemayo.hateoas.entity.AccountType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Relation(value = "account", collectionRelation="accounts")
public class AccountModel extends RepresentationModel<AccountModel> {
	
	private Long id;

	private Double balance;
	
	private Double interestRate;
	
	private Date creationDate;
	
	private AccountType type;
	
	private List<OperationModel> operations;

}
