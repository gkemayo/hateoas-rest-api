package com.gkemayo.hateoas.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import lombok.Data;

@Data
@Entity
public class Account implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private Double balance;
	
	private Double interestRate;
	
	private Date creationDate;
	
	@Enumerated(EnumType.STRING)
	private AccountType type;
	
	@OneToMany
	@JoinColumn(name = "account_id")
	private List<Operation> operations;

}
