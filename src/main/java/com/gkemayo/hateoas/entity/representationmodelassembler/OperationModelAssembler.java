package com.gkemayo.hateoas.entity.representationmodelassembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gkemayo.hateoas.controller.OperationRestController;
import com.gkemayo.hateoas.entity.Operation;
import com.gkemayo.hateoas.entity.representationmodel.OperationModel;

@Component
public class OperationModelAssembler extends RepresentationModelAssemblerSupport<Operation,  OperationModel>  {
	
	public OperationModelAssembler() {
		super(OperationRestController.class, OperationModel.class);
	}
	
	@Override
	protected OperationModel instantiateModel(Operation operation) {
		OperationModel operationModel = new OperationModel();
		ObjectMapper mapper = new ObjectMapper();
		try {
			String operationJson = mapper.writeValueAsString(operation);
			operationModel = mapper.readValue(operationJson, OperationModel.class);
		} catch (JsonMappingException e) {
		} catch (JsonProcessingException e) {
		}
		return operationModel;
	}

	@Override
	public OperationModel toModel(Operation operation) {
		return createModelWithId(operation.getId(), operation);
	}
	
}
