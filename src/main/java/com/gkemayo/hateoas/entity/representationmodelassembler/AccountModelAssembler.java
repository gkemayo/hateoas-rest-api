package com.gkemayo.hateoas.entity.representationmodelassembler;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gkemayo.hateoas.controller.AccountRestController;
import com.gkemayo.hateoas.controller.OperationRestController;
import com.gkemayo.hateoas.entity.Account;
import com.gkemayo.hateoas.entity.representationmodel.AccountModel;

@Component
public class AccountModelAssembler extends RepresentationModelAssemblerSupport<Account,  AccountModel> {
	
	
	public AccountModelAssembler() {
		super(AccountRestController.class, AccountModel.class);
	}

	@Override
	protected AccountModel instantiateModel(Account account) {
		AccountModel accountModel = new AccountModel();
		ObjectMapper mapper = new ObjectMapper();
		try {
			String accountJson = mapper.writeValueAsString(account);
			accountModel = mapper.readValue(accountJson, AccountModel.class);
			accountModel.getOperations().forEach(op -> 
					op.add(linkTo(methodOn(OperationRestController.class).operationById(op.getId())).withRel("operation_" + op.getId())));
		} catch (JsonMappingException e) {
		} catch (JsonProcessingException e) {
		}
		return accountModel;
	}

	@Override
	public AccountModel toModel(Account account) {
		return createModelWithId(account.getId(), account);
	}
	
}
