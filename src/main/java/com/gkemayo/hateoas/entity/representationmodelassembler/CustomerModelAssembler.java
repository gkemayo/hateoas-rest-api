package com.gkemayo.hateoas.entity.representationmodelassembler;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gkemayo.hateoas.controller.AccountRestController;
import com.gkemayo.hateoas.controller.CustomerRestController;
import com.gkemayo.hateoas.controller.OperationRestController;
import com.gkemayo.hateoas.entity.Customer;
import com.gkemayo.hateoas.entity.representationmodel.CustomerModel;

@Component
public class CustomerModelAssembler extends RepresentationModelAssemblerSupport<Customer, CustomerModel> {

	public CustomerModelAssembler() {
		super(CustomerRestController.class, CustomerModel.class);
	}

	@Override
	protected CustomerModel instantiateModel(Customer customer) {
		CustomerModel customerModel = new CustomerModel();
		ObjectMapper mapper = new ObjectMapper();
		try {
			String customerJson = mapper.writeValueAsString(customer);
			customerModel = mapper.readValue(customerJson, CustomerModel.class);
			customerModel.getAccounts().forEach(ac -> {
				ac.add(linkTo(methodOn(AccountRestController.class).accountById(ac.getId())).withRel("account_" + ac.getId()));
				ac.getOperations().forEach(op -> op
						.add(linkTo(methodOn(OperationRestController.class).operationById(op.getId())).withRel("operation_" + op.getId())));
			});
		} catch (JsonMappingException e) {
		} catch (JsonProcessingException e) {
		}
		return customerModel;
	}

	@Override
	public CustomerModel toModel(Customer customer) {
		return createModelWithId(customer.getId(), customer);
	}

}
