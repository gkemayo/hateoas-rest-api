package com.gkemayo.hateoas.entity;

public enum OperationType {
	
	DEBIT, CREDIT;
	
}
