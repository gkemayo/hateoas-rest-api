package com.gkemayo.hateoas.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gkemayo.hateoas.entity.Customer;
import com.gkemayo.hateoas.entity.representationmodel.CustomerModel;
import com.gkemayo.hateoas.entity.representationmodelassembler.CustomerModelAssembler;
import com.gkemayo.hateoas.repository.CustomerRepository;

@RestController
@RequestMapping("/customer")
public class CustomerRestController {
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	private CustomerModelAssembler customerModelAssembler;
	
	@GetMapping(value = "/all", produces = MediaTypes.HAL_JSON_VALUE)
	public ResponseEntity<CollectionModel<CustomerModel>> allCustomers() {
		
		List<Customer> customers = customerRepository.findAll();
		CollectionModel<CustomerModel> customersModel = customerModelAssembler.toCollectionModel(customers);
		customersModel.add(linkTo(methodOn(CustomerRestController.class).allCustomers()).withRel("customers"));
		
		return new ResponseEntity<CollectionModel<CustomerModel>>(customersModel, HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}", produces = MediaTypes.HAL_JSON_VALUE)
	public ResponseEntity<CustomerModel> customerById(@PathVariable("id") Long id) {
		
		Optional<Customer> customer = customerRepository.findById(id);
		if(customer.isPresent()) {
			CustomerModel customerModel = customerModelAssembler.toModel(customer.get());
			return new ResponseEntity<CustomerModel>(customerModel, HttpStatus.OK);
		}
		return new ResponseEntity<CustomerModel>(new CustomerModel(), HttpStatus.NOT_FOUND);
	}
	

}
