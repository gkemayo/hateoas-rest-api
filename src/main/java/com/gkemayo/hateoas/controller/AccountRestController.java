package com.gkemayo.hateoas.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gkemayo.hateoas.entity.Account;
import com.gkemayo.hateoas.entity.representationmodel.AccountModel;
import com.gkemayo.hateoas.entity.representationmodelassembler.AccountModelAssembler;
import com.gkemayo.hateoas.repository.AccountRepository;

@RestController
@RequestMapping("/account")
public class AccountRestController {
	
	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private AccountModelAssembler accountModelAssembler;
	
	@GetMapping(value = "/all", produces = MediaTypes.HAL_JSON_VALUE)
	public ResponseEntity<CollectionModel<AccountModel>> allAccounts() {
		
		List<Account> accounts = accountRepository.findAll();
		CollectionModel<AccountModel> accountsModel = accountModelAssembler.toCollectionModel(accounts);
		accountsModel.add(linkTo(methodOn(AccountRestController.class).allAccounts()).withRel("accounts"));
		
		return new ResponseEntity<CollectionModel<AccountModel>>(accountsModel, HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}", produces = MediaTypes.HAL_JSON_VALUE)
	public ResponseEntity<AccountModel> accountById(@PathVariable("id") Long id) {
		
		Optional<Account> account = accountRepository.findById(id);
		if(account.isPresent()) {
			AccountModel accountModel = accountModelAssembler.toModel(account.get());
			//accountModel.add(linkTo(methodOn(AccountRestController.class).accountById(id)).withSelfRel());
			return new ResponseEntity<AccountModel>(accountModel, HttpStatus.OK);
		}
		return new ResponseEntity<AccountModel>(new AccountModel(), HttpStatus.NOT_FOUND);
	}

}
