package com.gkemayo.hateoas.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gkemayo.hateoas.entity.Operation;
import com.gkemayo.hateoas.entity.representationmodel.OperationModel;
import com.gkemayo.hateoas.entity.representationmodelassembler.OperationModelAssembler;
import com.gkemayo.hateoas.repository.OperationRepository;

@RestController
@RequestMapping("/operation")
public class OperationRestController {
	
	@Autowired
	private OperationRepository operationRepository;
	
	@Autowired
	private OperationModelAssembler operationModelAssembler;
	
	@GetMapping(value = "/all", produces = MediaTypes.HAL_JSON_VALUE)
	public ResponseEntity<CollectionModel<OperationModel>> allOperations() {
		
		List<Operation> operations = operationRepository.findAll();
		CollectionModel<OperationModel> operationsModel = operationModelAssembler.toCollectionModel(operations);
		operationsModel.add(linkTo(methodOn(OperationRestController.class).allOperations()).withRel("operations"));
		
		return new ResponseEntity<CollectionModel<OperationModel>>(operationsModel, HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}", produces = MediaTypes.HAL_JSON_VALUE)
	public ResponseEntity<OperationModel> operationById(@PathVariable("id") Long id) {
		
		Optional<Operation> operation = operationRepository.findById(id);
		if(operation.isPresent()) {
			OperationModel operationModel = operationModelAssembler.toModel(operation.get());
			return new ResponseEntity<OperationModel>(operationModel, HttpStatus.OK);
		}
		return new ResponseEntity<OperationModel>(new OperationModel(), HttpStatus.NOT_FOUND);
	}

}
