package com.gkemayo.hateoas.controller.client;

import java.net.URI;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.client.Traverson;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gkemayo.hateoas.entity.representationmodel.AccountModel;
import com.gkemayo.hateoas.entity.representationmodel.CustomerModel;
import com.gkemayo.hateoas.entity.representationmodel.OperationModel;

@RestController
@RequestMapping("/client")
public class HateoasRestClient {
	
	@GetMapping("/customers")
	public ResponseEntity<CollectionModel<CustomerModel>> customers() {
		Traverson traverson = new Traverson(URI.create("http://localhost:8080/customer/all"), MediaTypes.HAL_JSON);
		
		ParameterizedTypeReference<CollectionModel<CustomerModel>> customersType = new ParameterizedTypeReference<CollectionModel<CustomerModel>>() {};
		CollectionModel<CustomerModel> customersModel = traverson.follow("customers").toObject(customersType);
		
		return new ResponseEntity<CollectionModel<CustomerModel>>(customersModel, HttpStatus.OK);
	}
	
	@GetMapping("/accounts")
	public ResponseEntity<CollectionModel<AccountModel>> accounts() {
		Traverson traverson = new Traverson(URI.create("http://localhost:8080/account/all"), MediaTypes.HAL_JSON);
		
		ParameterizedTypeReference<CollectionModel<AccountModel>> accountType = new ParameterizedTypeReference<CollectionModel<AccountModel>>() {};
		CollectionModel<AccountModel> accountsModel = traverson.follow("accounts").toObject(accountType);
		
		return new ResponseEntity<CollectionModel<AccountModel>>(accountsModel, HttpStatus.OK);
	}
	
	@GetMapping("/operations")
	public ResponseEntity<CollectionModel<OperationModel>> operations() {
		Traverson traverson = new Traverson(URI.create("http://localhost:8080/operation/all"), MediaTypes.HAL_JSON);
		
		ParameterizedTypeReference<CollectionModel<OperationModel>> OperationType = new ParameterizedTypeReference<CollectionModel<OperationModel>>() {};
		CollectionModel<OperationModel> operationsModel = traverson.follow("operations").toObject(OperationType);
		
		return new ResponseEntity<CollectionModel<OperationModel>>(operationsModel, HttpStatus.OK);
	}

}
